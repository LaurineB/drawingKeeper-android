package com.laurine_baillet.eval.drawingkeeper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lb on 02/02/2018.
 */

public class Folder {
    private String title;
    private List<Drawing> drawings = new ArrayList<>();

    public Folder(String title) {
        this.title = title;
    }
    public Folder(String title, List<Drawing> drawings) {
        this.title = title;
        this.drawings = drawings;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Drawing> getDrawings() {
        return drawings;
    }

    public void setDrawings(List<Drawing> drawings) {
        this.drawings = drawings;
    }
}
