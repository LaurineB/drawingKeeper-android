package com.laurine_baillet.eval.drawingkeeper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lb on 02/02/2018.
 */

public class DrawingKeeperOpenHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "DrawingKeeper.db";
    public static final int DATABASE_VERSION = 2;

    public DrawingKeeperOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DrawingKeeperDatabaseContract.FolderEntry.SQL_CREATE_TABLE);
        db.execSQL(DrawingKeeperDatabaseContract.FolderEntry.SQL_CREATE_INDEX1);

        DatabaseDataWorker worker = new DatabaseDataWorker(db);
        worker.insertFolders();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 2) {
            db.execSQL(DrawingKeeperDatabaseContract.FolderEntry.SQL_CREATE_INDEX1);
        }
    }
}
