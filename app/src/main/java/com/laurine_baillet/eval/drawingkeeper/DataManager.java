package com.laurine_baillet.eval.drawingkeeper;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import com.laurine_baillet.eval.drawingkeeper.DrawingKeeperDatabaseContract.FolderEntry;

/**
 * Created by lb on 02/02/2018.
 */

public class DataManager {

    private static DataManager ourInstance = null;

    private List<Folder> folderArrayList = new ArrayList<>();

    public static DataManager getInstance() {
        if(ourInstance == null) {
            ourInstance = new DataManager();
            ourInstance.initializeFolders();
        }
        return ourInstance;
    }

    public static void loadFromDatabase(DrawingKeeperOpenHelper dbHelper) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        final String[] courseColumns = {
                FolderEntry.COLUMN_FOLDER_TITLE};
        final Cursor courseCursor = db.query(FolderEntry.TABLE_NAME, courseColumns,
                null, null, null, null, FolderEntry.COLUMN_FOLDER_TITLE + " DESC");
        loadFoldersFromDatabase(courseCursor);
    }
    private static void loadFoldersFromDatabase(Cursor cursor) {
        int courseTitlePos = cursor.getColumnIndex(FolderEntry.COLUMN_FOLDER_TITLE);

        DataManager dm = getInstance();
        dm.folderArrayList.clear();
        while(cursor.moveToNext()) {
            String title = cursor.getString(courseTitlePos);
            Folder folder = new Folder(title);

            dm.folderArrayList.add(folder);
        }
        cursor.close();
    }

    public String getCurrentUserName() {
        return "Laurine Baillet";
    }

    public String getCurrentUserEmail() {
        return "laurine.baillet@estiam.com";
    }

    public List<Folder> getFolders() {
        return folderArrayList;
    }

    public int createFolder(String title) {
        Folder folder = new Folder(title);
        folderArrayList.add(folder);
        return folderArrayList.size() - 1;
    }

    public int findFolder(Folder folder) {
        for(int index = 0; index < folderArrayList.size(); index++) {
            if(folder.equals(folderArrayList.get(index)))
                return index;
        }

        return -1;
    }

    private DataManager() {
    }

    //region Initialization code

    private void initializeFolders() {
        List<Drawing> drawingsFolder1 = new ArrayList<>();
        Drawing drawing1 = new Drawing("drawing 1");
        Drawing drawing2= new Drawing("drawing 2");
        drawingsFolder1.add(drawing1);
        drawingsFolder1.add(drawing2);

        Folder folder = new Folder("Document 1",drawingsFolder1);
        folderArrayList.add(folder);

        Folder folder2 = new Folder("Document 2",drawingsFolder1);
        folderArrayList.add(folder2);

        Folder folder3 = new Folder("Document 3",drawingsFolder1);
        folderArrayList.add(folder3);

        Folder folder4 = new Folder("Document 4",drawingsFolder1);
        folderArrayList.add(folder4);
    }

    //endregion
}
