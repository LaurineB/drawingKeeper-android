package com.laurine_baillet.eval.drawingkeeper;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.laurine_baillet.eval.drawingkeeper.DrawingKeeperDatabaseContract.FolderEntry;

/**
 * Created by lb on 02/02/2018.
 */

public class DatabaseDataWorker {
    private SQLiteDatabase mDb;

    public DatabaseDataWorker(SQLiteDatabase db) {
        mDb = db;
    }

    public void insertFolders() {
        insertFolder("Document 1");
        insertFolder("Document 2");
        insertFolder("Document 3");
        insertFolder("Document 4");
    }


    private void insertFolder(String title) {
        ContentValues values = new ContentValues();
        values.put(FolderEntry.COLUMN_FOLDER_TITLE, title);

        long newRowId = mDb.insert(FolderEntry.TABLE_NAME, null, values);
    }



}
