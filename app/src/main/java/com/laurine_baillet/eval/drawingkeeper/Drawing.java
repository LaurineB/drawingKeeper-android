package com.laurine_baillet.eval.drawingkeeper;

/**
 * Created by lb on 02/02/2018.
 */

public class Drawing {
    private String title;

    public Drawing() {

    }

    public Drawing(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
