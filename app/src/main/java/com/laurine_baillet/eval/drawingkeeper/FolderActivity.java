package com.laurine_baillet.eval.drawingkeeper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AnalogClock;
import android.widget.TabHost;


public class FolderActivity extends AppCompatActivity {

    public static final String FOLDER_ID = "com.laurine_baillet.eval.drawingkeeper.FOLDER_ID";

    private CanvasView customCanvas;
    private TabHost tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_folder);
        setSupportActionBar(toolbar);

        tabs=(TabHost)findViewById(R.id.tabhost);

        tabs.setup();

        TabHost.TabSpec spec=tabs.newTabSpec("canva");
        spec.setContent(R.id.canva);
        spec.setIndicator("Canva");
        tabs.addTab(spec);

        customCanvas = (CanvasView) findViewById(R.id.canva);
    }

    public void clearCanvas(View v) {
        customCanvas.clearCanvas();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_folder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add) {
            TabHost.TabSpec spec=tabs.newTabSpec("canva1");

            spec.setContent(new TabHost.TabContentFactory() {
                public View createTabContent(String tag) {
                    return(customCanvas);
                }
            });
            spec.setIndicator("Canva");
            tabs.addTab(spec);
            return true;

        } else if(id == R.id.action_clear) {
            clearCanvas(customCanvas);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
