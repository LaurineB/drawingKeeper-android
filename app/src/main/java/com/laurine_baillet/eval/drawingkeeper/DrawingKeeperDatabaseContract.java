package com.laurine_baillet.eval.drawingkeeper;

import android.provider.BaseColumns;

/**
 * Created by lb on 02/02/2018.
 */

public class DrawingKeeperDatabaseContract {
    private DrawingKeeperDatabaseContract() {} // make non-creatable

    public static final class FolderEntry implements BaseColumns {
        public static final String TABLE_NAME = "folder";
        public static final String COLUMN_FOLDER_TITLE = "folder_title";

        // CREATE INDEX course_info_index1 ON course_info (course_title)
        public static final String INDEX1 = TABLE_NAME + "_index1";
        public static final String SQL_CREATE_INDEX1 =
                "CREATE INDEX " + INDEX1 + " ON " + TABLE_NAME +
                        "(" + COLUMN_FOLDER_TITLE + ")";

        public static final String getQName(String columnName) {
            return TABLE_NAME + "." + columnName;
        }

        // CREATE TABLE folder (folder_id, folder_title)
        public static final String SQL_CREATE_TABLE =
                "CREATE TABLE " + TABLE_NAME + " (" +
                        _ID + " INTEGER PRIMARY KEY, " +
                        COLUMN_FOLDER_TITLE + " TEXT NOT NULL)";
    }

}
